## LDAP SERVER

## @kiliangp Curs 2022-2023

Podem trobar les imatges al Dockerhub de [kiliangp](https://hub.docker.com/u/kiliangp)

Podem trobar la documentació sobre la assignatura a https://gitlab.com/kiliangp/ldap23

ASIX M06-ASO Escola del treball de Barcelona

## LDAP SERVERS

* kiliangp/ldap22:base -> Server bàsic ldap, amb base de dades edt.org.

* kiliangp/ldap22:config -> Exemples de configuració i configuració dinàmica del servidor.

* kiliangp/ldap22:editat -> Servidor edt.org amb els usuaris identificats per uid, usuaris típics més usuaris user01-user10, passwd de manager xifrat i activant la base de dades cn=config.

