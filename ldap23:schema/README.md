# M06-ASO Pràctica (3) LDAP SCHEMA

Crear una nova imatge docker anomenada: ldap23:practica

1. Pujar-la al git
2. Pujar-la al dockerhub
3. Generar els README.md apropiats
* Crear un schema amb:
* Un nou objecte STRUCTURAL
* Un nou objecte AUXILIARU
* Cada objecte ha de tenir almenys 3 nous atributs.
* Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i binary per contenir documents pdf.
4. Crear una nova ou anomenada practica.
5. Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 
* Assegurar-se de omplir amb dades reals la foto i el pdf.
6. Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles
7. Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.
---

<br>
Fitxers a utilitzar: 

* Dockerfile 
* cotxes.schema 
* docker-compose.yml 
* edt-org.ldif 
* ldap.conf 
* slapd.conf 
* startup.sh

## DOCKERFILE
```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="ldapserver_practica 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 nmap tree vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

## COTXES.SCHEMA
```
# cotxes.schema 
# 
# x-carrosseria:
# 	x-nom
# 	x-descripcio
# 	x-foto
# 	x-pdf
#
# x-caracteristiques:
# 	x-sede
#	x-danyat
#	x-habilitats
#
#-------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-nom'
  DESC 'Nom del cotxe'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.2 NAME 'x-descripcio'
  DESC 'Descripció del cotxe'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.1.1.3 NAME 'x-foto'
  DESC 'Foto del cotxe'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)

attributetype (1.1.2.1.1.4 NAME 'x-pdf'
  DESC 'Explicacio amb fitxer pdf del cotxe'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
  SINGLE-VALUE)

attributetype (1.1.2.2.1.1 NAME 'x-sede'
  DESC 'Sede de la empresa de cotxe'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

attributetype (1.1.2.2.1.2 NAME 'x-danyat'
  DESC 'Indica si el cotxe esta danyat'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE)

attributetype (1.1.2.2.1.3 NAME 'x-habilitats'
  DESC 'Indica les habilitats del cotxe'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE)

#----------------------------------------

objectClass (1.1.2.1.1 NAME 'x-carrosseria'
  DESC 'carrosseria del cotxe'
  SUP TOP
  STRUCTURAL
  MUST x-nom
  MAY ( x-descripcio $ x-foto $ x-pdf ))

objectClass (1.1.2.2.1 NAME 'x-caracteristiques'
  DESC 'Caracteristiques del personatge'
  SUP TOP 
  AUXILIARY
  MUST ( x-sede $ x-danyat )
  MAY x-habilitats)
```

## DOCKER-COMPOSE.YML 
```
version: "2:"
services:
  ldap:
    image: kiliangp/ldap23:practica
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - mynet
  phpldapadmin:
    image: kiliangp/phpldapadmin:latest
    container_name: phpldapadmin
    hostname: phpldapadmin
    ports:
      - "80:80"
    networks:
      - mynet
networks:
  mynet:
``` 

## EDT-ORG.LDIF 
En aquest fitxer afegim la nova Organizationalunit amb nom de "practica" amb les noves entitats amb els seus respectius ObjectClass. 

```
dn: dc=edt,dc=org
objectClass: top
objectClass: dcObject
objectClass: organization
o: edt.org
dc: edt

# Nova ou que implantem en el nostre fitxer ldif.
dn: ou=practica,dc=edt,dc=org
objectClass: top
objectClass: organizationalunit
ou: practica
description: ou creada per a la practica d'schema


####
# Creació de les entitats
dn: x-nom=ferrari,ou=practica,dc=edt,dc=org
objectClass: x-carrosseria
objectClass: x-caracteristiques
x-nom: ferrari
x-descripcio: cotxe alta gama color vermell
x-foto:< file:/opt/docker/ferrari.jpeg
x-pdf:< file:/opt/docker/ferrari.pdf
x-sede: Maranello
x-danyat: FALSE
x-habilitats: carreres, potencia, gama, estil, classe

dn: x-nom=lamborghini,ou=practica,dc=edt,dc=org
objectClass: x-carrosseria
objectClass: x-caracteristiques
x-nom: lamborghini
x-descripcio: cotxe alta gama color groc
x-foto:< file:/opt/docker/lambo.jpeg
x-pdf:< file:/opt/docker/lambo.pdf
x-sede: Sant Agata Bolognese 
x-danyat: TRUE
x-habilitats: qualitat, segur, elegant, precis

dn: x-nom=maserati,ou=practica,dc=edt,dc=org
objectClass: x-carrosseria
objectClass: x-caracteristiques
x-nom: maserati
x-descripcio: cotxe alta gama color negre
x-foto:< file:/opt/docker/maserati.jpeg
x-pdf:< file:/opt/docker/maserati.pdf
x-sede: modena
x-danyat: FALSE
x-habilitats: rapid, simbolic, luxos, calid
```

## LDAP.CONF 
``` 
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI ldap://ldap.edt.org 
```

## SLAPD.CONF

Només tindrem els schemes necessaris per a la pràctica.
```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
include 	/opt/docker/cotxes.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
#----------------------------------------------------------------------
database config
rootdn "cn=Sysadmin,cn=config"
rootpw syskey
# ---------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
#----------------------------------------------------------------------
```

## STARTUP.SH 

```
#! /bin/bash

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd  -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 
```

## ORDRES PER A FER

```
docker build -t kiliangp/ldap23:practica . -> creem la imatge per a la pràctica
[+] Building 1.8s (13/13) FINISHED                                                        docker:default
 => [internal] load .dockerignore                                                                   0.0s
 => => transferring context: 2B                                                                     0.0s
 => [internal] load build definition from Dockerfile                                                0.0s
 => => transferring dockerfile: 457B                                                                0.0s
 => [internal] load metadata for docker.io/library/debian:latest                                    1.1s
 => [auth] library/debian:pull token for registry-1.docker.io                                       0.0s
 => [1/7] FROM docker.io/library/debian:latest@sha256:133a1f2aa9e55d1c93d0ae1aaa7b94fb141265d0ee3e  0.0s
 => [internal] load build context                                                                   0.1s
 => => transferring context: 3.40MB                                                                 0.0s
 => CACHED [2/7] RUN apt-get update                                                                 0.0s
 => CACHED [3/7] RUN apt-get install -y procps iproute2 nmap tree vim ldap-utils slapd less         0.0s
 => CACHED [4/7] RUN mkdir /opt/docker                                                              0.0s
 => [5/7] COPY * /opt/docker/                                                                       0.1s
 => [6/7] RUN chmod +x /opt/docker/startup.sh                                                       0.3s
 => [7/7] WORKDIR /opt/docker                                                                       0.0s
 => exporting to image                                                                              0.1s
 => => exporting layers                                                                             0.1s
 => => writing image sha256:a8dce1f4145a98126c67b81ac492862a9abe586cc559c14fc376b4305d2c1b25        0.0s
 => => naming to docker.io/kiliangp/ldap23:practica                                                 0.0s

------------------------

$ docker compose -f docker-compose.yml up -d -> engeguem els dos serveis a la vegada a través del docker compose.
[+] Running 2/2
 ✔ Container phpldapadmin  Started                                                                  0.1s 
 ✔ Container ldap.edt.org  Started                                                                  0.5s 
``` 

**Si volem desplegar les màquines desde les ordres corresponents podem fer:**

```
$ docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 --net mynet -d kiliangp/ldap23:practica 
b507d09256c2366fade80ab23161b717f93da56ebe8b4a665f9311c5975edaef

$ docker run --rm --name phpldapadmin -h phpldapadmin -p 80:80 --net mynet -d kiliangp/phpldapadmin:latest
529d8ab611b104bf4b884a9bf0801d75b82a40abbb10d17d952e7719824363d1
```

En el navegador:
1. http://localhost/phpldapadmin
* login DN -> cn=Manager,dc=edt,dc=org
* Password -> secret
