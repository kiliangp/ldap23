# M06-Pràctica 1. Activar el dimoni ldap en detach i que es quedi funcionant

* **V1**. Imatge amb els paquets: procps, iproute2, tree, nmap, vim, ldap-utils, slapd, less

1. **vim Dockerfile**
* Primerament el que farem és crear un fitxer Dockerfile per detallar els paquets a actualitzar  i descarregar, els serveis, els directoris a construir, els ports a obrir, els permisos a executar, els programes que iniciar, etc.
2. **vim startup.sh** 
* El que farem en aquest apartat és indicar les ordres necesàries amb els punts que demana la pràctica, per exemple, esborrar directoris de configuració, generar una configuració dinàmica, engegar el servei ldap i que es quedi en background, etc.
3. **vim edt-org.dif**
* En aquest fitxer el que farem és introduir les dades que es troban en un fitxer del gitlab de la matèria, aquestes dades conformen una base de dades d'alumnes.
4. **vim slapd.conf**
* En aquest altre fitxer, també introduïrem les dades d'un arxiu de gitlab de la matèria, aquest conté directoris que introduirà, és com fer un "IMPORT"
5. **docker build -t kiliangp/ldap23:base .
* Farem una imatge a partir del Dockerfile que hem creat
6. **docker run --rm -h ldap.edt.org -d kiliangp/ldap23:base**
* Iniciem el container en detach, perquè el dimoni quedi actiu, mentre que podem fer altres coses. 
7. **docker ps**
* Podem veure que el container està encès i a més el nom que automàticament li posen.
8. *docker exec -it reverent_elion /bin/bash
* Iniciem el container detach amb "exec", podem fer un "ls" per veure que tot funciona correctament
9. **docker tag kiliangp/ldap23:base kiliangp/ldap2023:base**
* Li posem una etiqueta degut a què ja existía un respositori en el meu compte amb aquell nombre
10. **docker push kiliangp/ldap2023:base**
* Pugem la imatge en el nostre repositori
