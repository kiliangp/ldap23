# M06-ASO Pràctica (2) LDAP EDITAT

Exercici per fer (2): edtasixm06/ldap22:editat
generar un sol fitxer ldif anomenat edt-org.ldif (usuaris0-11)
* Afegir en el fitxer dos usuaris i una ou nova inventada i     posar-los dins la nova ou.
* Modificar el fitxer edt.org.ldif  modificant dn dels usuaris     utilitzant en lloc del cn el uid per identificar-los (rdn).
* Configurar el password de Manager que sigui ‘secret’ però     encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).
* Propagar el port amb -p -P
* Editar els dos README, en el general afegir que tenim una nova imatge. En el de la imatge ldap22:editat
* Descriure els canvis i les ordres per posar-lo en marxa.
* Configurar la base de dades cn=config amb un usuari administrador anomenat syadmin i password syskey.
* Verifiqueu que sou capaços de modificar la configuració de la base de dades edt.org en calent modificant per exemple el passwd o el nom o els permisos.


## DOCKERFILE
```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="ldap:editat"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
## STARTUP.SH 

```
#! /bin/bash
# Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0

```

## EDT-ORG.LDIF
```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: uid=Pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=Pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: uid=Anna,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: uid=Marta,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: uid=Jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 100
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: uid=Admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 10
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

# ---------------------------------------------------------------
# Contra passwd per tots els usuaris=user01

dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: usuari numero 1
sn: usuari numero 1
homephone: 555-222-2226
mail: user01@edt.org
description: usuari del cicle d'Asix
ou: 2asix
uid: user01
uidNumber: 777
gidNumber: 710
homeDirectory: /tmp/home/2asix/user01
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user02
cn: usuari numero 2
sn: usuari numero 2
homephone: 555-222-2227
mail: user02@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user02
uidNumber: 778
gidNumber: 711
homeDirectory: /tmp/home/2asix/user02
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user03
cn: usuari numero 3
sn: usuari numero 3
homephone: 555-222-2228
mail: user03@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user02
uidNumber: 779
gidNumber: 712
homeDirectory: /tmp/home/2asix/user03
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user04
cn: usuari numero 4
sn: usuari numero 4
homephone: 555-222-2229
mail: user04@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user04
uidNumber: 780
gidNumber: 713
homeDirectory: /tmp/home/2asix/user04
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user05
cn: usuari numero 5
sn: usuari numero 5
homephone: 555-222-2230
mail: user05@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user05
uidNumber: 781
gidNumber: 714
homeDirectory: /tmp/home/2asix/user05
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN


dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user06
cn: usuari numero 6
sn: usuari numero 6
homephone: 555-222-2231
mail: user06@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user06
uidNumber: 782
gidNumber: 715
homeDirectory: /tmp/home/2asix/user06
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN


dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user07
cn: usuari numero 7
sn: usuari numero 7
homephone: 555-222-2232
mail: user07@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user07
uidNumber: 783
gidNumber: 715
homeDirectory: /tmp/home/2asix/user07
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user08
cn: usuari numero 8
sn: usuari numero 8
homephone: 555-222-2233
mail: user08@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user08
uidNumber: 784
gidNumber: 716
homeDirectory: /tmp/home/2asix/user08
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN


dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user09
cn: usuari numero 9
sn: usuari numero 9
homephone: 555-222-2234
mail: user09@edt.org
description: alumne del cicle d'Asix 
ou: 2asix
uid: user09
uidNumber: 785
gidNumber: 717 
homeDirectory: /tmp/home/2asix/user09
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN


dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user10
cn: usuari numero 10
sn: usuari numero 10
homephone: 555-222-2235
mail: user10@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user10
uidNumber: 786
gidNumber: 718
homeDirectory: /tmp/home/2asix/user10
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

# --------------------------------------------------------

dn: ou=gamers,dc=edt,dc=org
ou: gamers
description: jugadors de la play
objectclass: organizationalunit

dn: cn=Jack Sparrow,ou=gamers,dc=edt,dc=org
objectClass: inetOrgPerson
cn: Jack Sparrow
cn: Jack Sparrow
sn: Sparrow
homephone: 666-777-9999
mail: jack@edt.org

dn: cn=Benzema Vini,ou=gamers,dc=edt,dc=org
objectClass: inetOrgPerson
cn: Benzema Vini
cn: Benzema Vini
sn: Vini
homephone: 111-222-3333
mail: vini@edt.org
```
* Afegim els usuaris dintre del fitxer "edt-org.ldif" i a més creem una organització "gamers" amb dos usuaris "Benzema Vini" i "Jack Sparrow"

## SLAPD.CONF
```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/duaconf.schema
include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
#include		/etc/ldap/schema/ppolicy.schema
include		/etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
# rootpw secret
rootpw {SSHA}iApqI30usBRO+UGJlBdHB89jOji8HPbK
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ---------------------------------------------------------------------
database config
rootdn "cn=Sysadmin,cn=config"
rootpw syskey
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

## MODICIACIONS1.LDIF 
```
dn: uid=user03,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: cn
cn: Vini 
```

## MODIFICACIONS2.LDIF 
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: new_passwd
-
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * write

```

## ORDRES PER MODIFICAR EN CALENT
```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f modificacions1.ldif
    # El que hem fet és identificar-nos amb -D com a "Sysadmin.config" (administrador de la BD config), estem indicant que la clau d'aquesta és "syskey" i que faci les modificacions a partir del fitxer "modificacions2.ldif".

ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'uid=user03,ou=usuaris,dc=edt,dc=org'
    # Comprovem que el registre s'ha canviat a través de la búsqueda del registre.
dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
sn: usuari numero 3
homephone: 555-222-2228
mail: user03@edt.org
description: alumne del cicle d'Asix
ou: 2asix
uid: user02
uidNumber: 779
gidNumber: 712
homeDirectory: /tmp/home/2asix/user03
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN
cn: Vini
```
* Podem fer-ho tant com a l'administrador de la base de dades config, administradorde la base de dades edt.org, com usuari de la base de dades (en aquest cas hem d'identificar-nos)

```
ldapmodify -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f modificacions1.ldif
modifying entry "uid=user03,ou=usuaris,dc=edt,dc=org"

ldapmodify -x -D 'uid=user04,ou=usuaris,dc=edt,dc=org' -w user01 -f modificacions1.ldif
modifying entry "uid=user04,ou=usuaris,dc=edt,dc=org"

cat modificacions1.ldif 
    dn: uid=user04,ou=usuaris,dc=edt,dc=org
    changetype: modify
    replace: homePhone
    homePhone: 777-777-7777
        - Hem cambiat el fitxer perquè modifiqui les dades del propi usuari

```
<br>
<br>


```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f modificacions2.ldif
    # El que hem fet és fer una modificació a través de xifrar la sortida amb "-x", ens hem identificat com administrador de la BD config i hem introduït la clau, que en aquest cas és "syskey". -> Fem modificacions a través d'un fitxer

ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcRootPW olcAccess
    dn: olcDatabase={1}mdb,cn=config
    olcRootPW: new_passwd
    olcAccess: {0}to * by * write
    - El que hem fet és buscar en una BD específica, que és "olcDatabase={1}mdb,cn=config' i que ens mostri el RootPW i el olcAccess, efectivament ens ha fet les modificacions
########################
Antiga sortida:
    dn: olcDatabase={1}mdb,cn=config
    olcAccess: {0}to *  by self write  by * read
    olcRootPW: {SSHA}iApqI30usBRO+UGJlBdHB89jOji8HPbK
```
* Podem veure que ens ha modificat el "cn" en calent a partir d'un fitxer.

# ORDRES QUE HEM FET DURANT LA PRÀCTICA

Per saber l'encriptació de la paraula "secret"; quan iniciem el container farem l'ordre:
```
slappassw -s secret
```

Per iniciar el container:
```
docker build -t kiliangp/ldap:editat .
docker run --rm --name editat -h ldap.edt.org -p 389:389 -d kiliangp/ldap:editat
docker exec -it editat /bin/bash
```
