# M06-ASO Pràctica (5) LDAP SCHEMA

Crear una nova imatge ldap: edtasixm06/ldap23:grups
1. Fet també que sigui la imatge latest edtasixm06/ldap23:latest 
2. Modificar el fitxer edt.org.ldif per afegir una ou grups.
3. Definir els següents grups:

* alumnes(600), professors(601), 1asix(610), 2asix(611),
sudo(27), 1wiam(612), 2wiam(613), 1hiaw(614).

4. Els grups han de ser elements posixGroup
5. Verificar el llistat dels usuaris i grups i la coherència de dades entre
6. Els usuaris que ja teníem i els nous grups creats.
7. Modificar el startup.sh perquè el servei ldap escolti per tots els
protocols: ldap ldaps i ldapi.

<br>
Fitxers a utilitzar:

* Dockerfile 
* edt-org.ldif 
* ldap.conf 
* slapd.conf 
* startup.sh

## DOCKERFILE

**Creem el fitxer que farà que es creï el nostre container**

```
# Ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="Kilian Steven Guanoluisa Pionce"
LABEL subject="pràctica ldap grup"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```
## EDT-ORG.LDIF

**Fitxer que té tota la informació de la base de dades "edt.org".**

```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris
objectclass: organizationalunit

dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectclass: organizationalunit


####################################################
# La contrasenya del usuaris és el seu propi nom
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Aumnes
uid: pau
uidNumber: 5000
gidNumber: 600
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 601
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Profes
uid: anna
uidNumber: 5002
gidNumber: 601
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: uid=jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
uid: jordi
uidNumber: 5004
gidNumber: 600
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3



####################################################
# Totes les contrasenyes de user és "user01"

dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user01
cn: Kilian
sn: Kilian S
homephone: 666-666-0001
mail: user01@edt.org
description: alumne de 1asix
ou: 1asix
uid: user01
uidNumber: 6001
gidNumber: 610
homeDirectory: /tmp/home/1asix/user01
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user02
cn: Cesar
sn: Cesar A
homephone: 666-666-0002
mail: user02@edt.org
description: alumne de 1asix
ou: 1asix
uid: user02
uidNumber: 6002
gidNumber: 610
homeDirectory: /tmp/home/1asix/user02
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user03
cn: Marti
sn: Marti R
homephone: 666-666-0003
mail: user03@edt.org
description: alumne de 1asix
ou: 1asix
uid: user02
uidNumber: 6003
gidNumber: 610
homeDirectory: /tmp/home/1asix/user03
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user04
cn: Ton 
sn: Ton R
homephone: 666-666-0004
mail: user04@edt.org
description: alumne de 1asix
ou: 1asix
uid: user04
uidNumber: 6004
gidNumber: 610
homeDirectory: /tmp/home/1asix/user04
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user05
cn: Tomas
sn: Tomas C
homephone: 666-666-0005
mail: user05@edt.org
description: alumne de 1asix
ou: 1asix
uid: user05
uidNumber: 6005
gidNumber: 610
homeDirectory: /tmp/home/1asix/user05
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user06
cn: Iber
sn: Iber T
homephone: 666-666-0006
mail: user06@edt.org
description: alumne de 2asix
ou: 2asix
uid: user06
uidNumber: 6006
gidNumber: 611
homeDirectory: /tmp/home/2asix/user06
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user07
cn: Bryan A
sn: Bryan isx
homephone: 666-666-0007
mail: user07@edt.org
description: alumne de 2asix
ou: 2asix
uid: user07
uidNumber: 6007
gidNumber: 611
homeDirectory: /tmp/home/2asix/user07
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user08
cn: Ivan
sn: Ivan P
homephone: 666-666-0008
mail: user08@edt.org
description: alumne de 2asix
ou: 2asix
uid: user08
uidNumber: 7008
gidNumber: 611
homeDirectory: /tmp/home/2asix/user08
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user09
cn: Isaac
sn: Isaac G
homephone: 666-666-0009
mail: user09@edt.org
description: alumne de 2asix
ou: 2asix
uid: user09
uidNumber: 7009
gidNumber: 611
homeDirectory: /tmp/home/2asix/user09
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: user10
cn: Saray
sn: Saray C
homephone: 666-666-0010
mail: user10@edt.org
description: alumne de 2asix
ou: 2asix
uid: user10
uidNumber: 7010
gidNumber: 611
homeDirectory: /tmp/home/2asix/user10
userPassword: {SSHA}i+zxvD8hgSVDEXQW0N/li/R2xA11znBN

###################################################################
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Profesors que treballen a l'Escola del Treball
memberUid: anna
memberUid: pere


dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Alumnes que cursen en l'Escola del Treball
memberUid: pau
memberUid: marta
memberUid: jordi

dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Clase de primer en el cicle superior d'ASIX
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05  

dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Clase de segon en el cicle superior d'ASIX
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: master
gidNumber: 27
description: Usuaris que estan en el grup sudoers
memberUid: admin

dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Clase d'ASIX que esta a la clase 1wiam

dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Clase d'ASIX que esta a la clase 2wiam

dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Clase d'ASIX que esta a la clase 1hiaw
```
## LDAP.CONF

**Fitxer que conté configuracions que afecten el comportament del client LDAP en el sistema. D'aquesta manera, podem especificar a que base de dades apunta per defecte i sobre que host.**

1. En el nostre cas:
* BASE 'dc=edt,dc=org' -> base de dades per defecte
* URI ldap://ldap.edt.org  -> ubicació del servidor LDAP al que el client es pot connectar.

```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI ldap://ldap.edt.org 
```
## SLAPD.CONF 

**Fitxer on tenim els schemes necessaris per a fer la pràctica (5 schemes en aquest cas).**

```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
#contra = secret
rootpw {SSHA}Sh8b/TLyQVPc1AaKU2Gu1+S5ntWiKSjC
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database config
rootdn "cn=sysadmin,cn=config"
rootpw syskey
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

## STARTUP.SH 

**Fitxer que s'executarà degut a la comanda del "Dockerfile".**

```
#! /bin/bash
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d 
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi:///' -> estem dient que estem escoltant pels  protocols "ldap","ldaps","ldapi"
```

## LLISTAT USUARIS

**Podem veure que hem canviat el RDN dels usuaris, abans estaven amb "cn", ,ara amb "uid".**

```
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Alumnes
uid: pau
uidNumber: 5000
gidNumber: 600              -> Podem veure que hem afegit un gidnumber específic, aquest número pertany a un grup
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 601            
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+
...
```

## OU=GRUPS
```
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectclass: organizationalunit
```

## GRUP ALUMNES

```
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Alumnes que cursen en l'Escola del Treball
memberUid: pau
memberUid: marta
memberUid: jordi
```

## GRUP PROFES
```
dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Profesors que treballen a l'Escola del Treball
memberUid: anna
memberUid: pere
```

## GRUP 1ASIX
```
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix
gidNumber: 610
description: Clase de primer en el cicle superior d'ASIX
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05 
```

## GRUP 2ASIX
```
dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Clase de segon en el cicle superior d'ASIX
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
```

## GRUP SUDO
```
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: master
gidNumber: 27
description: Usuaris que estan en el grup sudoers
memberUid: admin
```
## GRUP 1WIAM
```
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Clase d'ASIX que esta a la clase 1wiam
```

## GRUP 2WIAM
```
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Clase d'ASIX que esta a la clase 2wiam
```

## GRUP 1HIAW 
```
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Clase d'ASIX que esta a la clase 1hiaw
```

## PER ENCENDRE EL CONTENIDOR

```
$ docker run --rm --name ldap23 -h ldap.edt.org --net 2hisx -p 389:389 -p 639 -d kiliangp/ldap23:latest

$ docker exec -it ldap23 /bin/bash

```

## PROVES

**Per saber que per defecte apunta a una base de dades (edt.org)**

```
DINTRE DEL CONTAINER:
# ldapsearch -x -LLL

FORA DEL CONTAINER:
$ ldapsearch -x -LLL -b 'dc=edt,dc=org'
```

**Busquem els usuaris que hi han dintre de 2asix**

```
DINTRE DEL CONTAINER:
# ldapsearch -x -LLL 'cn=2asix' 'memberUid'
dn: cn=2asix,ou=grups,dc=edt,dc=org
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10

------------------------------------------

FORA DEL CONTAINER:
$ ldapsearch -x -LLL -b 'dc=edt,dc=org' 'cn=2asix' 'memberUid'
dn: cn=2asix,ou=grups,dc=edt,dc=org
memberUid: user06
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
```

**Busquem que hi ha dintre del grup de profes**

```
$ ldapsearch -x -LLL 'cn=professors' 'memberUid'
dn: cn=professors,ou=grups,dc=edt,dc=org
memberUid: anna
memberUid: pere
```

**Per assegurar-nos que els protocols 'ldap, ldaps, ldapi' estàn escoltant. Podem veure que està el port 639, per tant, està escoltant correctament.**

```
ss  -tulpn | grep slapd
tcp   LISTEN 0      2048         0.0.0.0:389        0.0.0.0:*    users:(("slapd",pid=14,fd=6))
tcp   LISTEN 0      2048         0.0.0.0:636        0.0.0.0:*    users:(("slapd",pid=14,fd=8))
tcp   LISTEN 0      2048            [::]:389           [::]:*    users:(("slapd",pid=14,fd=7))
tcp   LISTEN 0      2048            [::]:636           [::]:*    users:(("slapd",pid=14,fd=9))

```
